import React from 'react';
import Home from './views/Home';

class App extends React.Component {
  render() {
    return (
      <div>
        <h1 style={{ textAlign: 'center' }}>Online Test</h1>
        <Home />
      </div>
    );
  }
}

export default App;