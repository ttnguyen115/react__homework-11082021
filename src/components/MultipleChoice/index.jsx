import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createAction } from '../../store/actions';
import { actionType } from '../../store/actions/type';

class MultipleChoice extends Component {
    handleSelectAnswer = (e, answer) => {
        const newSelectedAnswers = [...this.props.answerList];
        const index = newSelectedAnswers.findIndex(answer => answer.QuestionId === e.target.name);
        
        if (index === -1) {
            const newAnswer = { QuestionId: e.target.name, answer: { content: answer.content, exact: answer.exact } };
            newSelectedAnswers.push(newAnswer);
            this.props.dispatch(createAction(actionType.SET_ANSWERS, newSelectedAnswers));
        } else {
            const newAnswer = { ...this.props.answerList[index], answer: { content: answer.content, exact: answer.exact } };
            this.props.dispatch(createAction(actionType.CHANGE_ANSWER, newAnswer));
        }
    }

    render() {
        const { id, content, answers } = this.props.quiz;
        return (
            <div>
                <h4>Question {id}: {content}</h4>
                {
                    answers.map(answer => (
                        <div className="answer" key={answer.id}>
                            <label>
                                <input 
                                    type="radio" 
                                    id={answer.id} 
                                    name={id} 
                                    value={answer.content} 
                                    onChange={(e) => this.handleSelectAnswer(e, answer)} 
                                />
                                {answer.content}
                            </label>
                        </div>
                    ))
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => ({ answerList: state.quiz.answerList });

export default connect(mapStateToProps)(MultipleChoice);