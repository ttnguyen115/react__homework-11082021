import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createAction } from '../../store/actions';
import { actionType } from '../../store/actions/type';

class ShortAnswer extends Component {
    handleSubmit = (e) => {
        const newAnswers = [...this.props.answerList];
        const index = newAnswers.findIndex(answer => answer.QuestionId === e.target.id);
        const answerCheck = e.target.value === this.props.quiz.answers[0].content;

        if (index === -1) {
            const newAnswer = { QuestionId: e.target.id, answer: { content: e.target.value, exact: answerCheck } };
            newAnswers.push(newAnswer);
            this.props.dispatch(createAction(actionType.SET_ANSWERS, newAnswers));
        } else {
            const newAnswer = { ...this.props.answerList[index], answer: { content: e.target.value, exact: answerCheck } };
            this.props.dispatch(createAction(actionType.CHANGE_ANSWER, newAnswer));
        }
    }

    render() {
        const { id, content } = this.props.quiz;
        return (
            <div>
                <h4>Question {id}: {content}</h4>
                <input 
                    id={id}
                    type="text" 
                    style={{ padding: 10, width: '50%', outline: 'none' }} 
                    onBlur={this.handleSubmit}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({ answerList: state.quiz.answerList });

export default connect(mapStateToProps)(ShortAnswer);