import { actionType } from "../actions/type";

const initialState = {
    quizList: [],
    answerList: [],
    correctCount: 0
}

const quizReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.SET_QUESTIONS:
            state.quizList = action.payload;
            return { ...state };

        case actionType.SET_ANSWERS: 
            state.answerList = action.payload;
            return { ...state };
        
        case actionType.CHANGE_ANSWER: 
            state.answerList = state.answerList.map(answer => (
                (answer.QuestionId === action.payload.QuestionId)
                ? { ...answer, answer: action.payload.answer }
                : answer
            ));
            return { ...state };

        case actionType.SUBMIT: 
            state.correctCount = action.payload;
            state.answerList.forEach(item => item.answer.exact && state.correctCount++);
            alert(`Correct answers: ${state.correctCount}`);
            return { ...state };

        default:
            return state;
    }
}

export default quizReducer;