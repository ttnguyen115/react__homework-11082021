import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createAction } from '../../store/actions';
import { fetchQuiz } from '../../store/actions/quiz';
import { actionType } from '../../store/actions/type';
import MultipleChoice from './../../components/MultipleChoice/index';
import ShortAnswer from './../../components/ShortAnswer/index';

class Home extends Component {
    componentDidMount() {
        this.props.dispatch(fetchQuiz);
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.dispatch(createAction(actionType.SUBMIT, 0));
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                {
                    this.props.quizList.map(quiz => (
                        quiz.questionType === 1
                        ? <MultipleChoice key={quiz.id} quiz={quiz} />
                        : <ShortAnswer key={quiz.id} quiz={quiz} />
                    ))
                }

                <button>Submit</button>
            </form>
        );
    }
}

const mapStateToProps = (state) => ({ quizList: state.quiz.quizList })

export default connect(mapStateToProps)(Home);